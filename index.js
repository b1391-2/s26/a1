// Import http module

let http = require("http");

// Create a variable port and assign the value of 3000

let port = 3000;

//create a server using create server method that will listen in to the port provided above.

const server = http.createServer((request, response)=>{

	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Welcome to login Page!')
	} else {
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Im sorry the page you are looking for cannot be found.')
	}

}).listen(port);


// Console log in the terminal a message when the server is successfully running.
console.log(`Server is running at localhost: ${port}`);